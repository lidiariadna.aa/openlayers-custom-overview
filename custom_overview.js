import './style.css';
import {Map, View} from 'ol';
import OSM from 'ol/source/OSM.js';
import TileLayer from 'ol/layer/Tile.js';
import {
  DragRotateAndZoom,
  defaults as defaultInteractions,
} from 'ol/interaction.js';
import {OverviewMap, defaults as defaultControls} from 'ol/control.js';

/*Elements that make up the rotate.*/
const rotateWithView = document.getElementById('rotateWithView');

/* Create an overview to the map.*/
const overviewMapControl = new OverviewMap({
  className: 'ol-overviewmap ol-custom-overviewmap',
  layers: [
    new TileLayer({
      source: new OSM({
        'url':
          'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=47633047370b40a297c9bb6d078ae86d',
      }),
    }),
  ],
  collapseLabel: '\u00BB',
  label: '\u00AB',
  collapsed: false,
});

rotateWithView.addEventListener('change', function () {
  overviewMapControl.setRotateWithView(this.checked);
});

/*Create the map.*/
const map = new Map({
  controls: defaultControls().extend([overviewMapControl]),
  interactions: defaultInteractions().extend([new DragRotateAndZoom()]),
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
  ],
  target: 'map',
  view: new View({
    center: [-11035828.104, 2206160.1144],
    zoom: 7,
  }),
});