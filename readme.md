# OpenLayers + Custom Overview

Los mapas suelen contener un mapa general más pequeño que muestra la ubicación actual dentro de la extensión total más grande. Con *_OpenLayers_*, esta funcionalidad se logra usando el control `OpenLayers.Control.OverviewMap`.


Un control en _*OL*_ es un componente interactivo o informativo, como un boton o un indicador, que se muestra en una posición fija sobre el mapa. Los controles se gestionan a través de objetos DOM y su posición se define mediante CSS. 


 Para crear controles personalizados, _*OL*_ ofrece la clase base `ol/control.js`. Se puede crear un control simplemente creando un elemento _*HTML*_ con los eventos(o _listeners_) deseados, iniciando el control con el siguiente elemento:

```js 
const miControl = nuevo Control({elemento: miElemento});
```
y luego agregar esto al mapa.

La principal ventaja de implementar estos componentes como controles, en lugar de usar elementos DOM ordinarios, incluye una mejor gestión de la interacción del usiario y la integración con la colección de controles del mapa. Esto permite acceder a métodos y propiedades útiles, facilitando la inclusión de funcionalidades como como:

- Attribution
- FullScreen
- MousePosition
- OverviewMap
- Rotate
- ScaleLine
- ZoomSlider
- ZoomToExtent
- Zoom

Especificamente, para este caso, se uso el control `OverviewMap`. Además, se integra la API de Thunderforest, para despues agregarlo a la vista del mapa.

```js 
const overviewMapControl = new OverviewMap({
  className: 'ol-overviewmap ol-custom-overviewmap',
  layers: [
    new TileLayer({
      source: new OSM({
        'url':
          'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=47633047370b40a297c9bb6d078ae86d',
      }),
    }),
  ],
  collapseLabel: '\u00BB',
  label: '\u00AB',
  collapsed: false,
```

Adicionalmente, se agregara una clase `ol/interaction/DragRotateAndZoom` la cual permitira a los usuarios hacer zoom y rotar el mapa haciendo clic y arrastrando en el mapa. De forma predeterminada, esta interacción se limita a cuando se mantiene presionada la tecla Mayús. Esta interacción solo es compatible con dispositivos de mouse.

```js
rotateWithView.addEventListener('change', function () {
  overviewMapControl.setRotateWithView(this.checked);
```

Finalmente, se integra la clase `ol/view` añadiendo el control y ajustando las especificaciones de la clase. Esto asegura que el control personalizado se visualice correctamente en el mapa y que los usiarios puedan interactuar de manera eficiente con el mapa, aprovechando las funcionalidades de zoom y rotación 

```js
const map = new Map({
  controls: defaultControls().extend([overviewMapControl]),
  interactions: defaultInteractions().extend([new DragRotateAndZoom()]),
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
  ],
  target: 'map',
  view: new View({
    center: [-11035828.104, 2206160.1144],
    zoom: 7,
  }),
});
```